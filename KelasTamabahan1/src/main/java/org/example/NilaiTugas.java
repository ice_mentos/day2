package org.example;

public class NilaiTugas {
    public void showTugas() {
        String[] nama={"Ardi","Pian", "Robi"};
        double[][] nilaiTugas = {
                {60.0F, 70.0F, 90.0F},
                {80.0F, 70.0F, 90.0F},
                {70.0F, 60.0F, 80.0F}};
        double nilai;

        System.out.println("+-------+-------+-------+-------+-------------+");
        System.out.println("|  Nama |  UTS  |  UAS  | TUGAS | NILAI AKHIR |");
        System.out.println("+-------+-------+-------+-------+-------------+");

        for (int i = 0; i < 3; i++){
            System.out.print("| " +  nama[i] + " \t|  ");
            for (int j = 0; j < 3; j++){
                System.out.print(nilaiTugas[i][j] + "\t| ");
            }
            nilai = (0.35 * nilaiTugas[i][0]) + (0.45 * nilaiTugas[i][1]) + (0.2 * nilaiTugas[i][2]);
            System.out.println(nilai + "\t      |");
        }
        System.out.println("+-------+-------+-------+-------+-------------+");
    }

    public static void main(String[] args) {
        NilaiTugas nilaiTugas = new NilaiTugas();
        nilaiTugas.showTugas();
    }
}
