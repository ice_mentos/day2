package org.example;

import java.util.Scanner;

public class BangunRuang{
    double p = 4;
    double l = 2;
    double t = 3;
    double phi = 3.14;
    double r = 7;
    double sisi = 5;
    double rBalok = p*l*t;
    double rBola  = phi*r*r*r*4/3;
    double rKubus = sisi*sisi*sisi;

    public void Balok(){
        System.out.println("Volume Balok = " + rBalok);
    }

    public void Bola() {
        System.out.println("Volume Bola = " + Math.round(rBola));
    }

    public void Kubus() {
        System.out.println("Volume Kubus = " + rKubus);
    }

    public void average() {
        double avg = rBalok + rKubus +  rBola/3;
        System.out.println("Rata rata semuanya adalah = " + Math.round(avg));
    }

    public void summary() {
        double sum = rBalok + rBola + rKubus;
        System.out.println("Jumlah Semua Bangun RUang = " + sum);
    }

    public static void main(String[] args) {
        BangunRuang bangunRuang = new BangunRuang();
        bangunRuang.Balok();
        bangunRuang.Bola();
        bangunRuang.Kubus();
        bangunRuang.average();
        bangunRuang.summary();
    }
}
