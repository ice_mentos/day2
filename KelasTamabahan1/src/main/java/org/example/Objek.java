package org.example;

class Hewan{
    String warna = "hitam";
}

public class Objek {
    //Objek adalah sesuatu yang memiliki status/state dan perilaku/kebiasaan/behaviour
    //Class adalah wadah atau tempat yang menampung objek dan method

    public static void main(String[] args) {
        //Membuat Objek
        Hewan hewan = new Hewan();
        System.out.println("Warna Kucing saya : " + hewan.warna);
    }
}
