package org.example;

class Mobil1{
    String merk = "BMW";
    int tahun = 2000;
    int kecepatan = 100;

    public void tambahKecepatan(int inputKecepatan){
        this.kecepatan += inputKecepatan;
        System.out.println("kecepatan setelah ditambah : " + kecepatan);
    }

    public void kurangiKecepatan(int kurangKecepatan){
        this.kecepatan -= kurangKecepatan;
        System.out.println("kecepatan setelah dikurangi : " + kecepatan);
    }
}

public class Mobil {
    public static void main(String[] args) {
        Mobil1 car2 = new Mobil1();
        System.out.println("Merk Mobil     : " + car2.merk);
        System.out.println("Tahun Produksi : " + car2.tahun);
        System.out.println("Kecaptan awal  : " + car2.kecepatan);

        Mobil1 car1 = new Mobil1();
        car1.tambahKecepatan(100);

        Mobil1 car = new Mobil1();
        car.kurangiKecepatan(50);
    }
}
